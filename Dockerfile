FROM python:3-slim as builder

RUN apt-get update -y
RUN pip install --root=/ --prefix=/usr/local ansible kubernetes
CMD ["/bin/sh"]